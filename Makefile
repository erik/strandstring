# This Makefile compiles and flashes four different programs:
# - led_on
# - button_contact
# - timer_test
# - button_toggle
# - button_serial
#
# To program a board, first make sure the PROGRAMMER variable is set correctly.
# (If you're using your own HTM(a)A programmer board, use usbtiny.) Each board
# will also have to have its fuses set once, which can be done by running
# "make program-fuses". Finally, run make program_<program_name>. For example,
# "make program_button_toggle" will build and flash the button_toggle program.

MMCU=attiny44
F_CPU = 20000000
CFLAGS=-mmcu=$(MMCU) -Wall -Os -DF_CPU=$(F_CPU) \
	-I external/OneWire \
	-I /usr/share/arduino/hardware/arduino/cores/arduino
#PROGRAMMER = atmelice_isp
PROGRAMMER = usbtiny

%.hex: %.elf
	avr-objcopy -O ihex $< $@;\
	avr-size --mcu=$(MMCU) --format=avr $@

#%.o: %.c
#	avr-g++ $(CFLAGS) -I./ -o $@ $<

program_%: %.hex
	avrdude -p t44 -P usb -c $(PROGRAMMER) -U flash:w:$<

external/OneWire/OneWire.o: external/OneWire/OneWire.cpp external/OneWire/OneWire.h
	avr-g++ -c $(CFLAGS) -I./ -o $@ $<

button_serial.o: button_serial.c external/OneWire/OneWire.h
	avr-g++ -c $(CFLAGS) -I./ -o $@ $<

button_serial.elf: button_serial.o OneWire.o
	avr-g++ $(CFLAGS) -o $@ $^

.PHONY: default
#default: program_button_serial
default: button_serial.hex

program-fuses:
	avrdude -p t44 -P usb -c $(PROGRAMMER) -U lfuse:w:0x5E:m
