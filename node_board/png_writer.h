#include <png.h>
#include <cstdint>

//--------------------------------------------------------------------------------------------------
// This class is derived from code by Guillaume Cottenceau, copyright 2002-2010 and distributed
// under the X11 license. https://gist.github.com/niw/5963798
class PngWriter {
public:
    PngWriter();
    ~PngWriter();

    void free();
    void allocate(int32_t width, int32_t height);
    png_bytep* row_pointers() { return row_pointers_; }
    void set_pixel(int32_t x, int32_t y, uint8_t value = 255);
    void set_all_pixels(uint8_t value);
    void set_all_pixels_black();
    void write(char const* filename);

private:
    uint32_t row_size() { return 3 * width_; }

    int32_t width_;
    int32_t height_;
    png_bytep* row_pointers_;
};
